const fs = require('fs');

let passwort = fs.readFileSync('./storage/auth');
// RUN TEST: npm run-script authtest
function changepw(newpw) {
  passwort = newpw;
  fs.writeFileSync('./storage/auth', newpw);
}
function pwok(zupruef) {
  if (zupruef === passwort) {
    return true;
  }
  return false;
}
function datas(pruf, data) {
  if (pruf === passwort) {
    return data;
  }
  return 'Passwort Check Fail!';
}
function ro() {
  return passwort;
}
module.exports = {
  changepw,
  ro,
  datas,
  pwok,
};
