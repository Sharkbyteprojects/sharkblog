FROM node:8
WORKDIR /usr/src/shark/blogger
COPY package*.json ./
RUN npm install
COPY . .
EXPOSE 80
CMD [ "npm", "start" ]