const express = require('express');
const morgan = require('morgan');
const helmet = require('helmet');
const exphbs = require('express-handlebars');
const fs = require('fs');
const bpars = require('body-parser');
const auth = require('./auth');

const app = express();
const blogs = [];// FORMAT: uber, picurl, text
const latest = 0;
app.use(helmet());
app.use(bpars.urlencoded({ extended: false }));
app.engine('handlebars', exphbs());
app.use(morgan('common', { immediate: true }));
app.set('view engine', 'handlebars');
// **********************************
app.get('/', (request, response) => {
  // MAIN
  response.render(`${__dirname}/pages/main.handlebars`, { blogs });
});
app.post('/login', (request, response) => {
  //  const ido = parseInt(request.params.id, 10);
  // const all = prochelp.becomenum(ido);
  const woks = auth.pwok(request.body.pwnet);
  if (woks) {
    response.render(`${__dirname}/pages/work.handlebars`, { blogs, pwor: request.body.pwnet });
  } else {
    response.redirect('/');
  }
});
app.post('/pwn', (request, response) => {
  //  const ido = parseInt(request.params.id, 10);
  // const all = prochelp.becomenum(ido);
  const woks = auth.pwok(request.body.pwnet);
  if (woks) {
    response.render(`${__dirname}/pages/pwn.handlebars`, { pwor: request.body.pwnet });
  } else {
    response.redirect('/');
  }
});
app.post('/pwn/n', (request, response) => {
  //  const ido = parseInt(request.params.id, 10);
  // const all = prochelp.becomenum(ido);
  const woks = auth.pwok(request.body.pwneto);
  if (woks) {
    if (request.body.pwnet == request.body.pwnett && request.body.pwnet != '' && request.body.pwnet != ' ') {
      auth.changepw(request.body.pwnet);
      response.redirect('/#complete,pwchanged');
    } else {
      response.redirect('/#fail,fail');
    }
  } else {
    response.redirect('/');
  }
});
app.post('/add', (request, response) => {
  //  const ido = parseInt(request.params.id, 10);
  // const all = prochelp.becomenum(ido);
  const woks = auth.pwok(request.body.pwnet);
  if (woks) {
    response.render(`${__dirname}/pages/add.handlebars`, { pwor: request.body.pwnet });
  } else {
    response.redirect('/');
  }
});
app.post('/save', (request, response) => {
  //  const ido = parseInt(request.params.id, 10);
  // const all = prochelp.becomenum(ido);
  const woks = auth.pwok(request.body.pwnet);
  if (woks) {
    let toadd = {
      uber: request.body.head,
      text: request.body.txt,
      picurl: request.body.img,
    };
    blogs[latest] = toadd;
    toadd++;
    response.redirect('/#complete');
  } else {
    response.redirect('/');
  }
});
app.use(express.static(`${__dirname}/content`)); // STATIC
app.delete('/*', (request, response) => { //                                                 ^
  const urlenc = request.path;//                                                       |
  response.status(404);//                                                              |
  response.render(`${__dirname}/pages/404.handlebars`, { mode: 'delete', url: urlenc });// 404
});
app.put('/*', (request, response) => { //                                                 ^
  const urlenc = request.path;//                                                       |
  response.status(404);//                                                              |
  response.render(`${__dirname}/pages/404.handlebars`, { mode: 'put', url: urlenc });// 404
});
app.get('/*', (request, response) => { //                                                 ^
  const urlenc = request.path;//                                                       |
  response.status(404);//                                                              |
  response.render(`${__dirname}/pages/404.handlebars`, { mode: 'get', url: urlenc });// 404
}); app.post('/*', (request, response) => { //                                            ^
  const urlenc = request.path;//                                                       |
  response.status(404);//                                                              |
  response.render(`${__dirname}/pages/404.handlebars`, { mode: 'post', url: urlenc });// 404
});
app.copy('/*', (request, response) => { //                                            ^
  const urlenc = request.path;//                                                       |
  response.status(404);//                                                              |
  response.render(`${__dirname}/pages/404.handlebars`, { mode: 'copy', url: urlenc });// 404
});
app.listen(80, () => {
  console.log("ONLINE");
});
